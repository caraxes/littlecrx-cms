<?php
//session start
session_start();


header ('Content-type: text/html; charset=utf-8');
//print_R($_SESSION);
// set session life time
ini_set('session.gc_maxlifetime', '300'); // 300 sec = 5 min

//autoload user-defined classes
      function __autoload($class){
          include 'class/'.$class.'.class.php';
      }
	
error_reporting(E_ALL);
ini_set('display_errors', '1');
	 
define('H_O_ST', "");
define('U_S_ER', "");
define('P_A_SS', "");
define('D_B_AS', "");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Little CRXCMS</title>
	<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
	<script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>
</head>
<body>
<div class="content_top">
</div>
<div class="content">
	<div class="dane">
        
		<?php
		//print_r($_SESSION);
		$vars = new vars();
  		switch ($_GET['goto']) {
		case 'menu':
		if(!isset($_SESSION['user_id'])||!is_numeric($_SESSION['user_id']))
			echo 'Proszę się zalogować ! <a href="login"> logowanie</a>';
		else {
		
			$m = new menu(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
			$m->displayMenu();
			
			echo '<p><a href="logout">Wyloguj się !</a></p>';
		}
		break;
		case 'art':
		if(!isset($_SESSION['user_id'])||!is_numeric($_SESSION['user_id']))
			echo 'Proszę się zalogować !';
		else {
			echo '<p><a href="menu">WRÓĆ</a></p>';
			$m = new content(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
			$m->edit(intval($_GET['id']));
		}
		break;
		case 'login':
			$login = new login(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
			$login -> authorization();
			$vars = new vars();

			echo'
			<form action="login" method="post">
				<p class="customer_p"><label class="customer_label">Nick:</label><input name="nick" type="text" value="'.$vars->show('nick').'" class="customer_text" /></p>
				<p class="customer_p"><label class="customer_label">Hasło:</label><input name="password" type="password" value="" class="customer_text" /></p>
				<p><img src="class/captcha.php" alt="CAPTCHA!" /> <span style="vertical-align:10px;">=</span> <input class="textfield_cap" name="cap" value="" /></p>
				<p class="customer_p"><input class="customer_button" value="Loguj" type="submit" /></p>
			</form>';
			
		break;
		case 'logout':
			
			$login = new login(H_O_ST, U_S_ER, P_A_SS, D_B_AS);
			$login -> logout();
			echo "<h1>Wylogowanie zakończone sukcesem</h1>";
			echo "Dziekujemy i zapraszamy ponownie";
		break;
		default:
			echo '<script>document.location = "login"</script>';
		break;
	  }
		
	?>    

  </div><!-- end dane -->
</div><!-- end content -->
<div class="footer">
<div class="fotter_txt">Little CRXCMS 2010 - <a href="http://www.webcrx.com" target="_blank">WebCRX</a></div>
</div>
</body>
</html>

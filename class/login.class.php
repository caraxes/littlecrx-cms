<?php
/**********
author : Łukasz 'caraxes' K. &copy; 2009
**********/
class login {

	  protected $host;
	  protected $user;
	  protected $pwd;
	  protected $dbName;
	 
	 
		 function __construct($host, $user, $pwd, $dbName){
			$this->host = $host;
			$this->user = $user;
			$this->pwd = $pwd;
			$this->dbName = $dbName;
		}

	public function authorization() {
	
		if(isset($_SESSION['user_id']))
			if(is_numeric($_SESSION['user_id']))
				echo "<script>document.location = 'menu'</script>";
	
		if(isset($_POST['nick']) and isset($_POST['password'])) {
		
		$formval = new formValidator();
		$formval -> validateEmpty('nick','Proszę podać nick');
		$formval -> validateEmpty('password','Proszę podać hasło',3,70);
		$formval -> validateCaptcha('cap','Proszę podać wynik z obrazka !');
		
		$formval_errors_number = $formval -> checkErrors();
			if($formval_errors_number > 0)
				echo $formval -> displayErrors();
		
		$clean = new clean();
		
			$email = $clean->czysc(trim($_POST['nick']));
			$password = $clean->czysc(trim(md5($_POST['password'])));
			
			$pdo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName.'', ''.$this->user.'', ''.$this->pwd.'', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
			if($formval_errors_number == 0) {
				$sql = $pdo->query("SELECT * from users WHERE users_name = '".$email."' AND users_pass = '".$password."'");
					$user_id = $sql -> fetch();
					$sql ->closeCursor();
					if($user_id['id_users'] != 0 && $user_id['users_status'] == 1) { 
					
							$_SESSION['user_ip'] = md5($_SERVER['REMOTE_ADDR']);
							$_SESSION['user_id'] = $user_id['id_users'];
						echo "<script>document.location = 'menu';</script>";
						
					} else echo "Błędne dane !<br /><br />";
			}
		}
	}
	
	public function logout() {

	session_unset();
	session_destroy();
	
	echo "<script>document.location = 'login';</script>";
	}
}
	
?>
